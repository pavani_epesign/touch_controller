//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------
#include <SI_EFM8SB1_Register_Enums.h>                  // SFR declarations
#include "InitDevice.h"
// $[Generated Includes]
#include "cslib_config.h"
#include "cslib.h"
#include "led_gpio.h"
#include "profiler_interface.h"
#include "comm_routines.h"
#include "EFM8SB1_FlashPrimitives.h"
#include "EFM8SB1_FlashUtils.h"
// [Generated Includes]$

#define SPEED1          0x1808
#define SPEED2          0x1809
#define FAN1            0x1602
#define FAN2            0x1603
#define LIGHT1          0x1604
#define LIGHT2          0x1605

void
Device_Init ();
void
Application ();
void
Check_Touch_Pads ();
void
Fan1_Control ();
void
Fan2_Control ();
void
Light1_Control ();
void
Light2_Control ();
void
Speed_Up_Control ();
void
Speed_Down_Control ();
void
Previous_Touch_Events ();

SI_SBIT(LED1, SFR_P1, 7);
SI_SBIT(LED2, SFR_P1, 6);
SI_SBIT(LED3, SFR_P1, 5);
SI_SBIT(LED4, SFR_P1, 2);          // '1' means ON, '0' means OFF
SI_SBIT(LED5, SFR_P1, 1);
SI_SBIT(LED6, SFR_P1, 0);
SI_SBIT(LED7, SFR_P0, 3);
SI_SBIT(LED8, SFR_P2, 7);

uint8_t Touch_FAN1 = 0, Touch_FAN2 = 0;
uint8_t Touch_Light1 = 0, Touch_Light2 = 0, L1_ON = 0, L2_ON = 0;
uint8_t Touch_Speed_Up = 0, Touch_Speed_Down = 0;
int8_t speed_fan1 = 0, speed_fan2 = 0;

void
delay ()
{
  int i;
  for (i = 0; i < 25000; i++)
    {

    }
}
//-----------------------------------------------------------------------------
// main() Routine
// ----------------------------------------------------------------------------
void
main (void)
{
  Device_Init ();
  Application ();
}

void
Device_Init ()
{
  LED1 = 0;
  LED2 = 0;
  LED3 = 0;
  LED4 = 0;
  LED5 = 0;
  LED6 = 0;
  LED7 = 0;
  LED8 = 0;

  // Call hardware initialization routine
  enter_DefaultMode_from_RESET ();
  // Configures all peripherals controlled by capsense, including
  // the sensing block and port pins
  CSLIB_initHardware ();

  // Initializes the capsense variables and performs some scans to
  // initialize the baselines
  CSLIB_initLibrary ();
}

void
Application ()
{
  //FLASH_PageErase (FAN1);
  Previous_Touch_Events ();
  while (1)
    {
      CSLIB_update ();

      if (CSLIB_anySensorDebounceActive ())
        {
          Check_Touch_Pads ();
        }
      if (Touch_FAN1 == 1)
        {
          Fan1_Control ();
        }
      if (Touch_FAN2 == 1)
        {
          Fan2_Control ();
        }
      if (Touch_Light1 == 1)
        {
          Light1_Control ();
        }
      if (Touch_Light2 == 1)
        {
          Light2_Control ();
        }
      if (Touch_Speed_Up == 1)
        {
          Speed_Up_Control ();
        }
      if (Touch_Speed_Down == 1)
        {
          Speed_Down_Control ();
        }
    }
}

void
Speed_Up_Control ()
{
  if ((Touch_FAN1 == 0) || (Touch_FAN2 == 0))
    {
      Touch_Speed_Up = 0;
    }
}

void
Speed_Down_Control ()
{
  if ((Touch_FAN1 == 0) || (Touch_FAN2 == 0))
    {
      Touch_Speed_Down = 0;
    }
}

void
Light1_Control ()
{
  LED7 = !LED7;
  if (LED7 == 1)
    {
      L1_ON = 1;
    }
  else
    {
      L1_ON = 0;
    }
  FLASH_Clear (LIGHT1, 1);
  FLASH_ByteWrite (LIGHT1, L1_ON);
  Touch_Light1 = 0;
}

void
Light2_Control ()
{
  LED8 = !LED8;
  if (LED8 == 1)
    {
      L2_ON = 1;
    }
  else
    {
      L2_ON = 0;
    }
  FLASH_Clear (LIGHT2, 1);
  FLASH_ByteWrite (LIGHT2, L2_ON);
  Touch_Light2 = 0;
}

void
Previous_Touch_Events ()
{
  Touch_FAN1 = FLASH_ByteRead (FAN1);
  Touch_FAN2 = FLASH_ByteRead (FAN2);
  L1_ON = FLASH_ByteRead (LIGHT1);
  L2_ON = FLASH_ByteRead (LIGHT2);
  speed_fan1 = FLASH_ByteRead (SPEED1);
  speed_fan2 = FLASH_ByteRead (SPEED2);
  if (Touch_FAN1 == 1)
    {
      FAN1_Speed_Control (speed_fan1);
      Touch_FAN1 = 0;
    }
  if (Touch_FAN2 == 1)
    {
      FAN2_Speed_Control (speed_fan2);
      Touch_FAN2 = 0;
    }
  if (L1_ON == 1)
    {
      LED7 = 1;
      Touch_Light1 = 0;
    }
  if (L2_ON == 1)
    {
      LED8 = 1;
      Touch_Light2 = 0;
    }
}

void
Fan1_Control ()
{
  if (Touch_Speed_Up == 1)
    {
      if ((speed_fan1 != 0) && (speed_fan1 != 1) && (speed_fan1 != 2)
          && (speed_fan1 != 3) && (speed_fan1 != 4))
        {
          speed_fan1 = 0;
        }
      speed_fan1++;
      if (speed_fan1 > 4)
        {
          speed_fan1 = 4;
        }
      FLASH_Clear (SPEED1, 1);
      FLASH_ByteWrite (SPEED1, speed_fan1);
      FAN1_Speed_Control (speed_fan1);
      Touch_Speed_Up = 0;
      //LED2 =0;
    }
  if (Touch_Speed_Down == 1)
    {
      speed_fan1--;
      if (speed_fan1 < 0)
        {
          speed_fan1 = 0;
        }
      FLASH_Clear (SPEED1, 1);
      FLASH_ByteWrite (SPEED1, speed_fan1);
      FAN1_Speed_Control (speed_fan1);
      Touch_Speed_Down = 0;
    }
}

void
Fan2_Control ()
{
  if (Touch_Speed_Up == 1)
    {
      if ((speed_fan2 != 0) && (speed_fan2 != 1) && (speed_fan2 != 2)
          && (speed_fan2 != 3) && (speed_fan2 != 4))
        {
          speed_fan1 = 0;
        }
      speed_fan2++;
      if (speed_fan2 > 4)
        {
          speed_fan2 = 4;
        }
      FLASH_Clear (SPEED2, 1);
      FLASH_ByteWrite (SPEED2, speed_fan2);
      FAN2_Speed_Control (speed_fan2);
      Touch_Speed_Up = 0;
    }
  if (Touch_Speed_Down == 1)
    {
      speed_fan2--;
      if (speed_fan2 < 0)
        {
          speed_fan2 = 0;
        }
      FLASH_Clear (SPEED2, 1);
      FLASH_ByteWrite (SPEED2, speed_fan2);
      FAN2_Speed_Control (speed_fan2);
      Touch_Speed_Down = 0;
    }
}
void
Check_Touch_Pads ()
{
  uint8_t status = 1;
  if (CSLIB_isSensorDebounceActive (0))
    {
      Touch_FAN1 = 1;
      FLASH_ByteWrite (FAN1, Touch_FAN1);
      Touch_FAN2 = 0;
    }
  if (CSLIB_isSensorDebounceActive (4))
    {
      Touch_FAN2 = 1;
      FLASH_ByteWrite (FAN2, Touch_FAN2);
      Touch_FAN1 = 0;
    }
  if (status == read_Speed_Up ())
    {
      //LED2 = 1;
      Touch_Speed_Up = 1;
    }
  if (status == read_Speed_Down ())
    {
      //LED7 = 1;
      Touch_Speed_Down = 1;
    }
  if (status == read_touch_light1 ())
    {
      Touch_Light1 = 1;
      //FLASH_ByteWrite (LIGHT1, Touch_Light1);
    }
  if (status == read_touch_light2 ())
    {
      //LED7 = 1;
      Touch_Light2 = 1;
      //FLASH_ByteWrite (LIGHT2, Touch_Light2);
    }
}
