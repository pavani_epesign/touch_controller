/*
 * led_gpio.c
 *
 *  Created on: Feb 26, 2024
 *      Author: vgnsi
 */

#include <SI_EFM8SB1_Register_Enums.h>
#include "cslib.h"

SI_SBIT(LED1, SFR_P1, 7);
SI_SBIT(LED2, SFR_P1, 6);
SI_SBIT(LED3, SFR_P1, 5);
SI_SBIT(LED4, SFR_P1, 2);          // '1' means ON, '0' means OFF
SI_SBIT(LED5, SFR_P1, 1);
SI_SBIT(LED6, SFR_P1, 0);

void
FAN1_Speed_Control (uint8_t speed)
{
  switch (speed)
    {
    case 0:
      LED1 = 0;
      LED2 = 0;
      LED3 = 0;
      break;
    case 1:
      LED1 = 1;
      LED2 = 0;
      LED3 = 0;
      break;
    case 2:
      LED1 = 0;
      LED2 = 1;
      LED3 = 0;
      break;
    case 3:
      LED1 = 1;
      LED2 = 1;
      LED3 = 0;
      break;
    case 4:
      LED1 = 0;
      LED2 = 0;
      LED3 = 1;
      break;
    default:
      break;
    }
}

void
FAN2_Speed_Control (uint8_t speed)
{
  switch (speed)
    {
    case 0:
      LED4 = 0;
      LED5 = 0;
      LED6 = 0;
      break;
    case 1:
      LED4 = 1;
      LED5 = 0;
      LED6 = 0;
      break;
    case 2:
      LED4 = 0;
      LED5 = 1;
      LED6 = 0;
      break;
    case 3:
      LED4 = 1;
      LED5 = 1;
      LED6 = 0;
      break;
    case 4:
      LED4 = 0;
      LED5 = 0;
      LED6 = 1;
      break;

    default:
      break;
    }
}
int
read_Speed_Up ()
{
  int status = 0;
  if (CSLIB_isSensorDebounceActive (1) == 1)
    {
      while (!(status == 1))
        {
          CSLIB_update ();
          if (CSLIB_isSensorDebounceActive (1) == 0)
            {
              status = 1;    // considering key press after press releasing
            }

        }
    }
  return status;

}

int
read_Speed_Down ()
{
  int status = 0;
  if (CSLIB_isSensorDebounceActive (5) == 1)
    {
      while (!(status == 1))
        {
          CSLIB_update ();
          if (CSLIB_isSensorDebounceActive (5) == 0)
            {
              status = 1;    // considering key press after press releasing
            }
        }
    }
  return status;

}

int
read_touch_light1 ()
{
  int status = 0;
  if (CSLIB_isSensorDebounceActive (3) == 1)
    {
      while (!(status == 1))
        {
          CSLIB_update ();
          if (CSLIB_isSensorDebounceActive (3) == 0)
            {
              status = 1;    // considering key press after press releasing
            }
        }
    }
  return status;

}

int
read_touch_light2 ()
{
  int status = 0;
  if (CSLIB_isSensorDebounceActive (2) == 1)
    {
      while (!(status == 1))
        {
          CSLIB_update ();
          if (CSLIB_isSensorDebounceActive (2) == 0)
            {
              status = 1;    // considering key press after press releasing
            }
        }
    }
  return status;

}
