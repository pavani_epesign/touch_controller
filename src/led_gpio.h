/*
 * led_gpio.h
 *
 *  Created on: Feb 26, 2024
 *      Author: vgnsi
 */

#ifndef SRC_LED_GPIO_H_
#define SRC_LED_GPIO_H_

#include <stdint.h>

void
FAN1_Speed_Control (uint8_t speed);
void
FAN2_Speed_Control (uint8_t speed);
int
read_Speed_Up ();
int
read_Speed_Down ();
int
read_touch_light1 ();
int
read_touch_light2 ();

#endif /* SRC_LED_GPIO_H_ */
