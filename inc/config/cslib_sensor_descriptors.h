#ifndef _SENSOR_DESCRIPTORS_H
#define _SENSOR_DESCRIPTORS_H

void
outputsensorDescriptors (void);

#define HAS_SENSOR_DESCRIPTORS

// $[sensor descriptors]
#define SENSOR_DESCRIPTOR_LIST \
    "CS0.0", \
    "CS0.1", \
    "CS0.2", \
    "CS1.4", \
    "CS0.5", \
    "CS0.3", \
//  Skip center button since it is grounded by default
//  "CS1.4",
// [sensor descriptors]$

#endif

